<?php

/**
 * Define this Export UI plugin.
 */
$plugin = array(
  'schema' => 'oauth2_client_service_client',  // As defined in hook_schema().
  'access' => 'administer users',  // Define a permission users must have to access these pages.

  // Define the menu item.

  'menu' => array(
    'menu item' => 'oauth2-connections',
    'menu title' => 'Oauth2 Connections',
    'menu description' => 'Administer OAuth2 Connections.',
  ),


// Define user interface texts.
  'title singular' => t('connection'),
  'title plural' => t('connections'),
  'title singular proper' => t('Oauth2 connection'),
  'title plural proper' => t('Oauth2 connections'),


// Define the names of the functions that provide the add/edit forms.
  'form' => array(
    'settings' => 'oauth2_client_service_ctools_export_ui_form',
  ),
);

/**
 * Define the preset add/edit form.
 */
function oauth2_client_service_ctools_export_ui_form(&$form, &$form_state) {
  $preset = $form_state['item'];

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#description' => t('Optional human-readable name for the connection.'),
    '#default_value' => isset($preset->description) ? $preset->description : '',
  );

  $form['client_id'] = array(
    '#type' => 'textfield',
    '#length' => 255,
    '#title' => t('Client ID'),
    '#description' => t('Optional client ID'),
    '#default_value' => $preset->client_id,
  );

  $form['client_configuration'] = array(
    '#process' => array('oauth2_client_service_client_configuration_field'),
    '#required' => TRUE,
    '#type' => 'fieldset',
    '#default_value' => $preset->client_configuration,
  );


  $form['client_class'] = array(
    '#type' => 'hidden',
    '#default_value' => $preset->client_class,
  );

  $form['#submit'] = array('oauth2_client_service_client_configuration_submit');
}



/**
 * Processor for client_configuration subfields.
 */
function oauth2_client_service_client_configuration_field($element) {
  // Hidden values have to be here, so they'll be set in the DB.
  $element['token_endpoint'] = array(
    '#type' => 'hidden',
    '#default_value' => 'https://api.soundcloud.com/oauth2/token',
  );
  $element['auth_flow'] = array(
    '#type' => 'hidden',
    '#default_value' => 'server-side',
  );

  // User-configurable values.
  $element['authorization_endpoint'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Authorization endpoint'),
    '#description' => t('The remote resource against which we will authorize'),
    '#default_value' => $element['#default_value']['authorization_endpoint'],
  );
  $element['client_id'] = array(
    '#title' => t('Client ID'),
    '#description' => t('The client ID provided by the remote resource.'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => $element['#default_value']['client_id'],
  );
  $element['client_secret'] = array(
    '#title' => t('Client Secret'),
    '#description' => t('The client secret provided by the remote resource'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => $element['#default_value']['client_secret'],
  );
  // Build an HTTPS URL as a default value for redirect_uri.
  global $base_url;
  $url = parse_url($base_url);
  $baseurl_default = 'https://' . $url['host'];

  $element['redirect_uri'] = array(
    '#title' => t('Redirect URI'),
    '#description' => t('The Redirect URI you gave to the remote resource for this site. Should almost always be the HTTPS address for this site, with path \'oauth2/authorized\''),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => !empty($element['#default_value']['redirect_uri']) ? $element['#default_value']['redirect_uri'] : $baseurl_default . '/oauth2/authorized',
  );

  unset($element['#default_value']);
  return $element;
}

/**
 * Submit handler for the config form, to unflatten client_configuration values.
 */
function oauth2_client_service_client_configuration_submit($form, &$form_state) {
  $values = $form_state['values'];
  $client_configuration = array(
    'token_endpoint' => $values['token_endpoint'],
    'auth_flow' => $values['auth_flow'],
    'authorization_endpoint' => $values['authorization_endpoint'],
    'client_id' => $values['client_id'],
    'client_secret' => $values['client_secret'],
    'redirect_uri' => $values['redirect_uri'],
  );
  $form_state['item']->client_configuration = $client_configuration;
  // Add description field
  $form_state['item']->description = $values['description'];
}