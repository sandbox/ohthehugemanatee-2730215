<?php

/**
 * @file Contains the OAuth2ClientService's Connection class.
 */

namespace Drupal\oauth2_client_service\OAuth2ClientService;

use Drupal\service_container\Legacy\Drupal7;

/**
 * Class Connection
 * @see OAuth2\Client
 */
class Connection extends \Oauth2\Client {
  /**
   * @var string
   */
  protected $server_url;

  /**
   * @var Drupal7
   */
  protected $drupal7;

  /**
   * Construct a Connection object.
   * @param Drupal7 $drupal7
   * @param array $params
   * @param string $id
   */
  public function __construct(Drupal7 $drupal7, $params = NULL, $id = NULL) {
    parent::__construct($params, $id);
    $this->drupal7 = $drupal7;

    if (isset($this->params['server_url'])) {
      $this->server_url = $params['server_url'];
    }

    // Get the token data from storage, assuming it exists.
    $this->token = $this->getStoredToken() + $this->token;
  }

  public function attachAuthorization(&$parameters) {
    // provide OAuth2 authorization header
    // cf. RFC 6750: https://tools.ietf.org/html/rfc6750#section-2.1
    $parameters['headers']['Authorization'] = 'Bearer ' . $this->getAccessToken();
  }

  /**
   * Stores an OAuth token. By default, the storage is $_SESSION, but subclasses
   * may extend this to use, e.g., site-wide storage with a Drupal variable.
   *
   * @param array $token
   */
  protected function storeToken($token) {
    $_SESSION['ouath2_client']['token'][$this->id] = $token;
  }

  /**
   * Retrieves a pre-existing token from storage. By default, the storage is $_SESSION,
   * but we allow clients to override this.
   *
   * @return array
   */
  protected function getStoredToken() {
    if (isset($_SESSION['oauth2_client']['token'][$this->id])) {
      return $_SESSION['oauth2_client']['token'][$this->id];
    }

    return array();
  }

  /**
   * Get and return an access token.
   *
   * If the existing token hasn't expired, return that. Otherwise, we request
   * a new one.
   *
   * This method has been overridden from its parent to allow for custom token
   * storage code paths.
   */
  public function getAccessToken() {
    // XXX copypasta

    // Check wheather the existing token has expired.
    // We take the expiration time to be shorter by 10 sec
    // in order to account for any delays during the request.
    // Usually a token is valid for 1 hour, so making
    // the expiration time shorter by 10 sec is insignificant.
    // However it should be kept in mind during the tests,
    // where the expiration time is much shorter.
    $expiration_time = $this->token['expiration_time'];
    if ($expiration_time > (time() + 10)) {
      // The existing token can still be used.
      return $this->token['access_token'];
    }

    try {
      // Try to use refresh_token.
      $token = $this->getTokenRefreshToken();
    }
    catch (\Exception $e) {
      // Get a token.
      switch ($this->params['auth_flow']) {
        case 'client-credentials':
          $token = $this->getToken(array(
            'grant_type' => 'client_credentials',
            'scope' => $this->params['scope'],
          ));
          break;

        case 'user-password':
          $token = $this->getToken(array(
            'grant_type' => 'password',
            'username' => $this->params['username'],
            'password' => $this->params['password'],
            'scope' => $this->params['scope'],
          ));
          break;

        case 'server-side':
          $token = $this->getTokenServerSide();
          break;

        default:
          throw new \Exception(t('Unknown authorization flow "!auth_flow". Suported values for auth_flow are: client-credentials, user-password, server-side.',
            array('!auth_flow' => $this->params['auth_flow'])));
          break;
      }
    }
    $token['expiration_time'] = REQUEST_TIME + $token['expires_in'];

    // this is the only difference between us and the parent method
    $this->token = $token;
    $this->storeToken($token);

    // Redirect to the original path (if this is a redirection
    // from the server-side flow).
    self::redirect();

    // Return the token.
    return $token['access_token'];
  }

  /**
   * Performs an HTTP request
   * @param string $method An HTTP verb (e.g., GET or POST)
   * @param string $endpoint The endpoint path
   * @param array $parameters
   * @return \stdClass
   * @see drupal_http_request
   */
  public function request($method, $endpoint, array $parameters = array()) {
    $this->attachAuthorization($parameters);
    $parameters['method'] = $method;

    if (!url_is_external($endpoint)) {
      $endpoint = $this->server_url . $endpoint;
    }

    return $this->drupal7->drupal_http_request($endpoint, $parameters);
  }

  /**
   * Perform a redirect to a previously-saved path.
   *
   * We have overridden (read: pasted) the original method and modified it
   * slightly to use drupal_get_query_parameters() instead of using $_REQUEST
   * bare. We also don't use $_REQUEST period, because in some configurations
   * it can leak cookies. Plus there's a bug.
   *
   * \Oauth2\Client has a bug in that the ::redirect method trusted
   * $_REQUEST without filtering out things like 'q', so it may or may not
   * clobber the clean url we pass in. (i.e., /foo?q=bar can redirect to 'bar'
   * instead of 'foo'). The end result is that we get an infinite
   * redirect loop.
   *
   * Original bug: https://www.drupal.org/node/2345337
   * Docs on $_REQUEST: https://secure.php.net/manual/en/reserved.variables.request.php
   */
  public static function redirect($clean =TRUE) {
    if (!isset($_REQUEST['state'])) {
      watchdog('oauth2_client_service', 'No state detected.', array(), WATCHDOG_ERROR);
      return;
    }
    $state = $_REQUEST['state'];

    if (!isset($_SESSION['oauth2_client']['redirect'][$state])) {
      watchdog('oauth2_client_service', 'No state detected in oauth2_client redirect.', array(), WATCHDOG_ERROR);
      return;
    }
    $redirect = $_SESSION['oauth2_client']['redirect'][$state];

    // list of query params to ignore
    // ('clean' redirects lose the OAuth code and state params)
    $filter = array('q');
    if ($clean) {
      $filter[] = 'code';
      $filter[] = 'state';
    }

    // obtain some, but not all, of the query parameters
    $params = drupal_get_query_parameters(NULL, $filter);
    $params = $redirect['params'] + $params;

    // this block probably can be refactored
    if ($redirect['client'] != 'oauth2_client') {
      unset($_SESSION['oauth2_client']['redirect'][$state]);
      drupal_goto($redirect['uri'],
        array('query' => $params));
    }
    else {
      if ($clean) {
        unset($_SESSION['oauth2_client']['redirect'][$state]);
      }
      drupal_goto($redirect['uri'],
        array('query' => $params));
    }
  }
}