<?php

namespace Drupal\oauth2_client_service\OAuth2ClientService;

use Drupal\service_container\Legacy\Drupal7;

class Controller {
  /**
   * @var Drupal7
   */
  protected $drupal7;

  public function __construct(Drupal7 $drupal7) {
    $this->drupal7 = $drupal7;
  }

  /**
   * Loads a configuration instance
   * @param string $name
   * @return Connection If configuration was found, returns a Connection object. Returns NULL otherwise
   */
  public function getConnection($name) {
    ctools_include('export');
    $config = ctools_export_crud_load('oauth2_client_service_client', $name);

    if (!$config) {
      return NULL;
    }

    $class = '\Drupal\oauth2_client_service\OAuth2ClientService\Connection';
    if (isset($config->client_class)) {
      $class = $config->client_class;
    }

    $client_id = $name;
    if (isset($config->client_id)) {
      $client_id = $config->client_id;
    }

    return new $class($this->drupal7, $config->client_configuration, $client_id);
  }
}